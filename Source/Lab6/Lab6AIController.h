// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Lab6AIController.generated.h"

/**
 * 
 */
UCLASS()
class LAB6_API ALab6AIController : public AAIController
{
	GENERATED_BODY()

public:
	ALab6AIController();

	FORCEINLINE UBlackboardComponent* GetBlackboardComponent() const { return BlackboardComponent; }
	FORCEINLINE TArray<AActor*> GetPatrolPoints() const { return PatrolPoints; }
	FORCEINLINE int32 GetCurrentPatrolPoint() const { return CurrentPatrolPoint;  }

	UFUNCTION()
		void SetCurrentPatrolPoint(int32 NewPatrolPoint) { CurrentPatrolPoint = NewPatrolPoint; }

	UPROPERTY(EditDefaultsOnly)
		FName NextPointKey;

protected:
	UPROPERTY()
		TArray<AActor*> PatrolPoints; //matches values defined in Lab 6 AI

private:
	class UBehaviorTreeComponent* BehaviorComponent;
	class UBlackboardComponent* BlackboardComponent;

	int32 CurrentPatrolPoint; //The patrol point AI is currently going to

	virtual void OnPossess(APawn* InPawn) override;
	
};
