// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab6GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB6_API ALab6GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
