// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab6AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Lab6AI.h"

ALab6AIController::ALab6AIController()
{
	BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>
		(TEXT("BehaviorComponent"));
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>
		(TEXT("BlakboardComponent"));

	NextPointKey = "NextPoint";
	CurrentPatrolPoint = 0;
}

void ALab6AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	ALab6AI* AICharacter = Cast<ALab6AI>(InPawn);
	if (AICharacter)
	{
		if (AICharacter->BehaviorTree->BlackboardAsset)
		{
			BlackboardComponent->InitializeBlackboard(*(AICharacter->BehaviorTree->BlackboardAsset));
		}

		//Connect AI Character's patrol points to controller's Patrol Points
		PatrolPoints = AICharacter->GetPatrolPoints();
		BlackboardComponent->SetValueAsObject(NextPointKey, PatrolPoints[CurrentPatrolPoint]);

		BehaviorComponent->StartTree(*AICharacter->BehaviorTree);
	}
}