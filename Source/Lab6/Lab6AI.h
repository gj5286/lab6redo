// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab6AI.generated.h"

UCLASS()
class LAB6_API ALab6AI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab6AI();

	FORCEINLINE TArray<AActor*> GetPatrolPoints() const { return PatrolPoints; }

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
		class UBehaviorTree * BehaviorTree;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> PatrolPoints; 
};
