// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrolTaskMode.h"
#include "Lab6AIController.h"
#include "PatrolPoint.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UPatrolTaskMode::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	ALab6AIController* AIController = Cast<ALab6AIController>(OwnerComponent.GetAIOwner());

	if (AIController)
	{
		UBlackboardComponent* BlackboardComponent = AIController->GetBlackboardComponent();
		TArray<AActor*> AvailablePatrolPoints = AIController->GetPatrolPoints();

		APatrolPoint* NextPoint = Cast<APatrolPoint>(AvailablePatrolPoints[0]);

		int32 CurrentPatrolPoint = AIController->GetCurrentPatrolPoint();
		if (CurrentPatrolPoint != AvailablePatrolPoints.Num() - 1)
		{
			NextPoint = Cast<APatrolPoint>(AvailablePatrolPoints[++CurrentPatrolPoint]);
			AIController->SetCurrentPatrolPoint(CurrentPatrolPoint);
		}
		else
		{
			NextPoint = Cast<APatrolPoint>(AvailablePatrolPoints[0]);
			AIController->SetCurrentPatrolPoint(0);
		}
		
		BlackboardComponent->SetValueAsObject(AIController->NextPointKey, NextPoint);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}